import React from "react";
import ReactDOM from "react-dom";
// third party
import { BrowserRouter } from "react-router-dom";
// project imports
import App from "./App";
import reportWebVitals from "./reportWebVitals";

// ===========================|| REACT DOM RENDER  ||=========================== //

ReactDOM.render(
  <BrowserRouter>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
