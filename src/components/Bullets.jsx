function Bullets() {
  return (
    <div className="Bullets">
      {[...Array(5)].map((e, i) => {
        return <span key={i}></span>;
      })}
    </div>
  );
}
export default Bullets;
