import { useMemo } from "react";
import { GoogleMap, useLoadScript, Marker } from "@react-google-maps/api";
import icon from "../styles/images/Marcador.png";
import iconActive from "../styles/images/MarcadorActivo.png";

export default function Home({ markers, selectedValue, setSelectedValue }) {
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });

  if (!isLoaded) return <div>Loading...</div>;
  return (
    <Map
      Marcadores={markers}
      selectedValue={selectedValue}
      setSelectedValue={setSelectedValue}
    />
  );
}

function Map({ Marcadores, selectedValue, setSelectedValue }) {
  const center = useMemo(
    () => ({ lat: 4.651866177586513, lng: -74.10188859386166 }),
    []
  );
  const CallBackMarker = (e) => {
    setSelectedValue(e);
  };
  return (
    <GoogleMap zoom={12} center={center} mapContainerClassName="map-container">
      {Marcadores?.map((marker) => {
        return (
          <Marker
            key={marker.id}
            onClick={(e) => {
              CallBackMarker(marker.id);
            }}
            icon={marker.id === selectedValue ? iconActive : icon}
            title={marker.title}
            position={marker.position}
          />
        );
      })}
    </GoogleMap>
  );
}
