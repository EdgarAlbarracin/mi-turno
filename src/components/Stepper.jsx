import Step from "@mui/material/Step";
import Bullets from "./Bullets";

function Stepper({ activeStep, steps }) {
  return (
    <div className="Steppers">
      {steps.map(
        (number) =>
          (number > 1 && (
            <div
              key={`content${number}`}
              className={number <= activeStep ? "active" : ""}
            >
              <Bullets key={`Bullet${number}`} className="Bullet" />
              <Step className="Step" key={`Step${number}`}>
                {number}
              </Step>
            </div>
          )) || (
            <div
              key={`content${number}`}
              className={number <= activeStep && "active"}
            >
              <Step className="Step" key={`Step${number}`}>
                {number}
              </Step>
            </div>
          )
      )}
    </div>
  );
}

export default Stepper;
