import { TextField, Autocomplete, styled } from "@mui/material";
import "./../styles/css/search.css";

const CssTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "green",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "green",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "gray",
    },
    "&:hover fieldset": {
      borderColor: "green",
    },
    "&.Mui-focused fieldset": {
      borderColor: "green",
    },
  },
});

export default function SearchInput({
  Marcadores,
  selectedValue,
  setSelectedValue,
}) {
  const handleAutocomplete = (event, Value) => {
    if (Value != null) {
      setSelectedValue(Value.id);
    }
  };

  return (
    <div className="TextField-without-border-radius">
      <Autocomplete
        freeSolo
        options={Marcadores.length > 0 ? Marcadores : []}
        value={
          selectedValue && Marcadores.length > 0
            ? Marcadores[selectedValue - 1]
            : 0
        }
        getOptionLabel={(Marcadores) => Marcadores?.title || ""}
        onChange={handleAutocomplete}
        renderInput={(params) => (
          <CssTextField {...params} label="Buscar" fullWidth />
        )}
      />
    </div>
  );
}
