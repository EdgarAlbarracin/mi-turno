import { Box, Button, Container, Grid } from "@mui/material";
import { useNavigate } from "react-router-dom";
import "../styles/css/index.css";
import netux from "../styles/images/netux.png";
import Banner from "../styles/images/Grupo2073.png";
// ===========================|| MAIN LAYOUT ||=========================== //

function Index() {
  const navigate = useNavigate();
  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: "0vw 7vw",
        height: "100%",
        position: "relative",
      }}
    >
      <Box className="infoSteps">
        <h1>
          Solicita tu turno virtual{" "}
          <span> y realiza todos tus trámites sin filas.</span>
        </h1>
        <h3>Como solicitar tu turno</h3>
        <Grid>
          <div>1</div>
          <p>Selecciona la oficina mas cercana.</p>
          <div>2</div>
          <p>Ingresa tus datos.</p>
          <div>3</div>
          <p>Selecciona el servicio.</p>
          <div>4</div>
          <p>Verifica tu información.</p>
        </Grid>
        <Button
          className="buttonStyled"
          onClick={() => {
            navigate(`/StepOne`);
          }}
        >
          Solicitar turno
        </Button>
      </Box>
      <Box className="BoxImages">
        <img loading="lazy" src={Banner} alt="Banner" />
        <img loading="lazy" src={netux} alt="Netux" />
      </Box>
    </Container>
  );
}

export default Index;
