import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { Box, Container } from "@mui/material";
import Stepper from "../../components/Stepper";
import { Outlet } from "react-router-dom";
import Button from "@mui/material/Button";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import "../../styles/css/Main.css";
import logo from "../../styles/images/CruzVerde.png";
import netux from "../../styles/images/netux.png";
// ===========================|| MAIN LAYOUT ||=========================== //

function MainLayout() {
  const navigate = useNavigate();
  const [activeStep, setactiveStep] = useState(0);
  const [data, setData] = useState(
    window.localStorage.getItem("data")
      ? JSON.parse(window.localStorage.getItem("data"))
      : {
          address: false,
          form: false,
        }
  );
  const { pathname } = useLocation();

  const CallBackSteps = (step) => {
    if (step === 4) {
      navigate(`/StepFour`);
    }
    if (step === 3) {
      navigate(`/StepThree`);
    }
    if (step === 2) {
      navigate(`/StepTwo`);
    }
    if (step === 1) {
      navigate(`/StepOne`);
    }
    if (step === 0) {
      navigate(``);
    }
  };

  useEffect(() => {
    window.localStorage.setItem("data", JSON.stringify(data));
  }, [data]);

  useEffect(() => {
    const PathSteps = () => {
      switch (pathname) {
        case "/StepOne":
          setactiveStep(1);
          break;
        case "/StepTwo":
          setactiveStep(2);
          break;
        case "/StepThree":
          setactiveStep(3);
          break;
        case "/StepFour":
          setactiveStep(4);
          break;
        default:
          setactiveStep(0);
      }
    };
    PathSteps();
  }, [pathname, setactiveStep]);

  return (
    <Container disableGutters maxWidth={false}>
      <Box className={activeStep > 0 ? "header active" : "header"}>
        <Box
          className="logo"
          onClick={() => {
            navigate("/");
          }}
        >
          <img loading="lazy" src={logo} alt="Cruz Verde" />
        </Box>
        {activeStep > 0 && (
          <Box className="actions">
            <Box>
              <Button
                variant="text"
                startIcon={<KeyboardBackspaceIcon />}
                onClick={() => {
                  CallBackSteps(activeStep === 0 ? activeStep : activeStep - 1);
                }}
              >
                Atras
              </Button>
            </Box>
            <Box>
              <Stepper activeStep={activeStep} steps={[1, 2, 3, 4]} />
            </Box>
            <Box className="netux">
              <img loading="lazy" src={netux} alt="Netux" />
            </Box>
          </Box>
        )}
      </Box>
      <Box className="Outlet">
        <Outlet context={[data, setData]} />
      </Box>
    </Container>
  );
}

export default MainLayout;
