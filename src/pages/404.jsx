import React from "react";
import { useNavigate } from "react-router-dom";
import { Grid } from "@mui/material";
import NotFound from "../styles/images/404.png";

function NotFoundPage() {
  const navigate = useNavigate();
  return (
    <Grid
      sx={{
        height: "100%",
        pb: "7vw",
        width: "100vw",
        background: "#FFF",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
      }}
      onClick={() => {
        navigate(`/`);
      }}
    >
      <img
        src={NotFound}
        alt=""
        sx={{ maxHeight: "100vh", maxWidth: "100%" }}
      />
    </Grid>
  );
}
export default NotFoundPage;
