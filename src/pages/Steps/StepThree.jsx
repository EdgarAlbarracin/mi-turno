import { useState, useEffect } from "react";
import { Backdrop, Box, Button, Container, Fade, Modal } from "@mui/material";
import { useNavigate, useOutletContext } from "react-router-dom";
import Banner from "../../styles/images/Grupo2073.png";
import CruzVerde from "../../styles/images/CruzVerde.png";
import { get, post } from "../../services/sedes_data.service";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  borderRadius: "1.5vw",
  border: "none",
  boxShadow: "0px 3px 6px #00000029",
  p: 4,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  textAlign: "center",
};

function Index() {
  const navigate = useNavigate();
  const [marker, setMarker] = useState([]);
  const [data, setData] = useOutletContext();
  const [open, setOpen] = useState(data?.Success ? true : false);
  // LLamada a la API
  useEffect(() => {
    async function fetchData() {
      try {
        if (!data.address) {
          return navigate(`/StepOne`);
        }
        const response = await get(data.address);
        setMarker(response);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
  }, [data, navigate]);

  const handleClose = () => {
    setOpen(false);

    setData(false);
    return navigate(`/`);
  };

  const RandomDate = (rangeOfDays, startHour, hourRange) => {
    const today = new Date(Date.now());
    const dateRandom = new Date(
      today.getYear() + 1900,
      today.getMonth(),
      today.getDate() + Math.random() * rangeOfDays,
      Math.random() * hourRange + startHour,
      Math.random() * 60
    );
    const fecha =
      dateRandom.getDate() +
      "-" +
      (dateRandom.getMonth() + 1) +
      "-" +
      dateRandom.getFullYear();
    const hora =
      dateRandom.getHours() +
      ":" +
      dateRandom.getMinutes() +
      ":" +
      dateRandom.getSeconds();
    return { fecha, hora };
  };
  const CallBackSubmit = async () => {
    const turno = Math.round(Math.random() * 999);
    const { fecha, hora } = RandomDate(30, 8, 2);
    const dataForm = {
      ...data.form,
      idadress: data.address,
      turno,
      fecha,
      hora,
    };
    const response = await post(dataForm);
    if (!response.status === 201) {
      return alert("Error al guardar los datos");
    }
    setData({ ...data, Success: response.data });
    setOpen(true);
    navigate(`/StepFour`);
  };

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="ModalSuccess" sx={style}>
            <img loading="lazy" src={CruzVerde} alt="Banner" />
            <h1>¡Has solicitado tu turno con éxito!</h1>
            <p>
              Servicio: <span>Consulta Externa</span>
            </p>
            <p>
              Dia: <span>{`${data?.Success?.fecha}`}</span>
            </p>
            <p>
              Hora: <span>{`${data?.Success?.hora}`}</span>
            </p>

            <Button
              className="buttonStyled"
              onClick={() => {
                handleClose();
              }}
            >
              Solicitar nuevo turno
            </Button>
          </Box>
        </Fade>
      </Modal>
      <Container
        disableGutters
        maxWidth={false}
        sx={{
          display: "flex",
          justifyContent: "space-between",
          padding: "0vw 7vw",
          height: "100%",
          position: "relative",
        }}
      >
        <Box className="ConfirmInfo">
          <h1>Verifica tu información</h1>
          <h1>
            {`${data.form.PrimerNombre} ${data.form.SegundoNombre} `} <br />{" "}
            {` ${data.form.PrimerApellido} ${data.form.SegundoApellido}`}
          </h1>
          <p>{`${data.form.TipoDocumento}:  ${data.form.Documento}`}</p>
          <h1>Consulta Externa</h1>
          <h1>{marker.title}</h1>
          <p>
            {marker.Direccion}
            <br />
            {marker.horario}
          </p>

          <Button
            className="buttonStyled"
            onClick={() => {
              CallBackSubmit();
            }}
          >
            Aceptar
          </Button>
        </Box>
        <Box className="BoxImages StepThree">
          <img loading="lazy" src={Banner} alt="Banner" />
        </Box>
      </Container>
    </>
  );
}

export default Index;
