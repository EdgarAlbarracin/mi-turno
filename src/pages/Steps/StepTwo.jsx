import { useState, useEffect } from "react";
import { useOutletContext } from "react-router-dom";
import {
  Box,
  Button,
  Container,
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Paper,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import "../../styles/css/steptwo.css";
import Banner from "../../styles/images/Step2.svg";
import { get } from "../../services/sedes_data.service";
// ===========================|| MAIN LAYOUT ||=========================== //

function Index() {
  const navigate = useNavigate();
  const [marker, setMarker] = useState([]);
  const [data, setData] = useOutletContext();
  const initialForm = {
    TipoDocumento: data?.form?.TipoDocumento || "",
    Documento: data?.form?.Documento || "",
    PrimerNombre: data?.form?.PrimerNombre || "",
    SegundoNombre: data?.form?.SegundoNombre || "",
    PrimerApellido: data?.form?.PrimerApellido || "",
    SegundoApellido: data?.form?.SegundoApellido || "",
  };
  const [form, setForm] = useState(initialForm);
  // LLamada a la API
  useEffect(() => {
    async function fetchData() {
      try {
        if (!data.address) {
          return navigate(`/StepOne`);
        }
        const response = await get(data.address);
        setMarker(response);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
  }, [data, navigate]);

  const CallBackForm = (e) => {
    if (
      !form.TipoDocumento ||
      !form.Documento ||
      !form.PrimerNombre ||
      !form.SegundoNombre ||
      !form.PrimerApellido ||
      !form.SegundoApellido
    ) {
      alert("Datos incompletos");
      return;
    }
    setData({ ...data, form });
    navigate(`/StepThree`);
  };

  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{
        display: "grid",
        gridTemplateColumns: "21vw 1fr",
        alignItems: "center",
        padding: "0vw 7vw",
        height: "100%",
        gridGap: "9vw",
      }}
    >
      <Box
        className="FormInputs"
        component="form"
        noValidate
        autoComplete="off"
      >
        <h1>Ingresa tus datos</h1>
        <Grid>
          <FormControl variant="standard">
            <InputLabel id="demo-simple-select-standard-label">
              Documento
            </InputLabel>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              value={form.TipoDocumento}
              onChange={(e) =>
                setForm({
                  ...form,
                  TipoDocumento: e.target.value,
                })
              }
              label="Documento"
              required
            >
              <MenuItem value={`CC`}>CC</MenuItem>
              <MenuItem value={`RC`}>RC</MenuItem>
              <MenuItem value={`TI`}>TI</MenuItem>
            </Select>
          </FormControl>
          <TextField
            label="Numero de documento"
            type="number"
            onChange={(e) =>
              setForm({
                ...form,
                Documento: e.target.value,
              })
            }
            value={form.Documento}
            InputLabelProps={{
              shrink: true,
              name: "Documento",
            }}
            variant="standard"
          />
          <TextField
            onChange={(e) =>
              setForm({
                ...form,
                PrimerNombre: e.target.value,
              })
            }
            value={form.PrimerNombre}
            label="Primer nombre"
            variant="standard"
          />
          <TextField
            onChange={(e) =>
              setForm({
                ...form,
                SegundoNombre: e.target.value,
              })
            }
            value={form.SegundoNombre}
            label="Segundo nombre"
            variant="standard"
          />
          <TextField
            onChange={(e) =>
              setForm({
                ...form,
                PrimerApellido: e.target.value,
              })
            }
            value={form.PrimerApellido}
            label="Primer apellido"
            variant="standard"
          />
          <TextField
            onChange={(e) =>
              setForm({
                ...form,
                SegundoApellido: e.target.value,
              })
            }
            value={form.SegundoApellido}
            label="Segundo apellido"
            variant="standard"
          />
        </Grid>
        <Grid className="listItemsAdress">
          <Paper>
            <p>{marker.title}</p>
            <p>{marker.Direccion}</p>
            <p>{marker.horario}</p>
          </Paper>
        </Grid>
        <Button
          className="buttonStyled"
          onClick={() => {
            CallBackForm();
          }}
        >
          Siguiente
        </Button>
      </Box>
      <Box className="BoxImage">
        <img loading="lazy" src={Banner} alt="Banner" />
      </Box>
    </Container>
  );
}

export default Index;
