import { useOutletContext } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { Box, Button, Container, Grid, Radio } from "@mui/material";
import { useState, useEffect, useRef } from "react";
import Map from "../../components/Map";
import Search from "./../../components/Search";
import { get } from "../../services/sedes_data.service";
import "./../../styles/css/StepOne.css";

// ===========================|| MAIN LAYOUT ||=========================== //

function Index() {
  const [data, setData] = useOutletContext();
  const navigate = useNavigate();

  const [selectedValue, setSelectedValue] = useState(data.address || 1);
  const [Marcadores, setMarcadores] = useState([]);
  const handleChange = (event) => {
    setSelectedValue(parseInt(event.target.value, 10));
  };

  // Radio Buttons Control
  const controlProps = (item) => ({
    checked: selectedValue === item,
    onChange: handleChange,
    value: item,
    name: "size-radio-button-demo",
  });
  const itemRef = useRef(null);

  // Scroll a Dirección seleccionada
  useEffect(() => {
    if (itemRef && itemRef.current) {
      const item = itemRef.current;

      item.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  }, [selectedValue]);

  // LLamada a la API
  useEffect(() => {
    async function fetchData() {
      try {
        const dataApi = await get();
        setMarcadores(dataApi);
      } catch (error) {
        console.log(error);
      }
    }
    fetchData();
  }, []);

  const CallBackAdress = () => {
    setData({ ...data, address: selectedValue });
    navigate(`/StepTwo`);
  };

  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{
        display: "grid",
        justifyContent: "space-between",
        height: "100%",
        gridTemplateColumns: "22vw 1fr",
      }}
    >
      <div className="frame">
        <Box>
          <h1>Selecciona la oficina mas cercana</h1>
          <Search
            Marcadores={Marcadores}
            selectedValue={selectedValue}
            setSelectedValue={setSelectedValue}
          />
          <Grid className="listItems ScrollStyled">
            {Marcadores?.map((marker) => {
              return (
                <Grid
                  key={marker.id}
                  onClick={() => {
                    setSelectedValue(marker.id);
                  }}
                  className={marker.id === selectedValue ? "active" : ""}
                  ref={marker.id === selectedValue ? itemRef : undefined}
                >
                  <Radio {...controlProps(marker.id)} size="small" />
                  <p>{marker.title}</p>
                  <p>{marker.Direccion}</p>
                  <p>{marker.horario}</p>
                </Grid>
              );
            })}
          </Grid>
          <Button
            className="buttonStyled"
            onClick={() => {
              CallBackAdress();
            }}
          >
            Seleccionar
          </Button>
        </Box>
      </div>
      <Box className="BoxMaps">
        <div className="google-map-code">
          <Map
            markers={Marcadores}
            selectedValue={selectedValue}
            setSelectedValue={setSelectedValue}
          />
        </div>
      </Box>
    </Container>
  );
}

export default Index;
