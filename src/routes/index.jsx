import React from "react";
import MainLayout from "../pages/MainLayout";
import NotFound from "../pages/404";
import Home from "../pages";
import StepOne from "../pages/Steps/StepOne";
import StepTwo from "../pages/Steps/StepTwo";
import StepThree from "../pages/Steps/StepThree";
import { useRoutes } from "react-router-dom";

// ===========================|| ROUTING RENDER ||=========================== //

export default function ThemeRoutes() {
  const MainRoutes = [
    {
      path: "",
      element: <MainLayout />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/StepOne",
          element: <StepOne />,
        },
        {
          path: "/StepTwo",
          element: <StepTwo />,
        },
        {
          path: "/StepThree",
          element: <StepThree />,
        },
        {
          path: "/StepFour",
          element: <StepThree />,
        },
        { path: "*", element: <NotFound /> },
      ],
    },
  ];

  const routes = useRoutes(MainRoutes);
  return routes;
}
