import React from "react";
// routing
import Routes from "./routes";

// ===========================|| APP ||=========================== //

function App() {
  return <Routes />;
}

export default App;
