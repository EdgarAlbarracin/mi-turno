# MI TURNO

_Proyecto MI TURNO_

Sistema de gestión de turnos para uso en consulta externa, atención al usuario, urgencias
  <br/><br/>

# Tabla de contenido 📌

- [Comenzando](#comenzando-🚀)
- [Instalación](#Instalación-🔧)
- [Ejecutar paquete en desarrollo 🤓](#ejecutar-paquete-en-desarrollo-🤓)

---

# Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira [Despliegue](#despliegue) para conocer como desplegar el proyecto.

## Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

### Preparación Ambiente Desarrollo

- [Node](https://nodejs.org/dist/v14.17.4/node-v14.17.4-x64.msi)

- Se Requiere la versión de npm 7

```shell
  npm install --global npm@
  ```

# Instalación 🔧

_Paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

- ## Clonar proyecto

  ```shell
  git config --global user.name "John Doe"
  git config --global user.email johndoe@example.com
  git clone https://gitlab.com/EdgarAlbarracin/mi-turno.git
  ```
- ## Instalar Paquetes desde la raiz
  ```shell
  cd mi-turno
  npm i
  ```
# Ejecutar paquete en desarrollo 🤓

```shell
npm run start
```
---
# Construido con 🛠️

_todos los paquetes instalados pueden ser vistos en los archivos **package.json**

- [react](https://reactjs.org/) - biblioteca para crear interfaces de usuario
- [react-router-dom](https://v5.reactrouter.com/) - Manejos de rutas dentro de React
- [@mui/material](https://mui.com/) - Framework UI basado en componentes de Google
- [@react-google-maps/api](https://react-google-maps-api-docs.netlify.app/) - Dependencia de la API de Google para la integración de Maps  
- [axios](https://www.npmjs.com/package/react-axios) - Dependencia para la solicitud de peticiones  
- [json-server](https://github.com/typicode/json-server) - API REST falsa para el manejo de peticiones


# Autor ✒️

_Menciona a todos aquellos que ayudaron a levantar este proyecto desde sus inicios_

- **Edgar Yezid Albarracín Cruz** - _Desarrollador Frontend React JS_
# Copyright

🍺 [EdgarAlbarracin](https://gitlab.com/EdgarAlbarracin) 😊

---

